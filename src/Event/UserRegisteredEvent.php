<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\User;

class UserRegisteredEvent extends Event
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user =  $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}

### In your code Controller ###
#<?php

##use Symfony\Component\EventDispatcher\EventDispatcherInterface;
##use App\Event\UserRegisteredEvent;

 ##  public function register(EventDispatcherInterface $eventDispatcher)
##{
 ##   $eventDispatcher->dispatch(new UserRegisteredEvent($user), 'user_registered');
##}

