<?php

namespace App\Controller;

use App\Entity\Movie;
use App\OmdbApi;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MovieController extends AbstractController
{
    private OmdbApi $omdbApi;

    public function __construct(OmdbApi $omdbApi)
    {
        $this->omdbApi = $omdbApi;
        //$this->omdbApi = new OmdbApi(HttpClient::create(),'28c5b7b1', 'https://www.omdbapi.com');
    }

    /**
     * @Route("/movie", name="movie")
     */
    public function index(): Response
    {
        return $this->render('movie/index.html.twig', [
            'controller_name' => 'MovieController',
        ]);
    }

    /**
     * @Route("/movie/{imdbId}/import", name="movie_import")
     */
    public function import($imdbId, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $movieData = $this->omdbApi->requestOneById($imdbId);

        $movie = Movie::fromApi($movieData);
        $entityManager->persist($movie);
        $entityManager->persist($movie);
        $entityManager->flush();

        return $this->redirectToRoute('movie_latest');
    }

    /**
     * @Route("/movie/{id}", name="movie_show", requirements={"id" : "\d+"})
     * @param int $id
     * @return Response
     */
    public function show(int $id, MovieRepository $movieRepository): Response
    {
        $movie = $movieRepository->findOneById($id);

        if ($this->isGranted('MOVIE_SHOW',$movie)) {
            throw new AccessDeniedException('Vous ne pouvez pas acceder à ce film '. $movie->getTitle());
        }
        return $this->render('movie/show.html.twig',[
            'movie' => $movie
        ]);
    }

    /**
     * @Route("/movie/latest", name="movie_latest")
     * @param MovieRepository $movieRepository
     * @return Response
     */
    public function latest(MovieRepository $movieRepository): Response
    {
        $movies = $movieRepository->findBy([], ['id' => 'DESC']);

        return $this->render('movie/latest.html.twig', [
            'movies' => $movies
        ]);
    }

    /**
     * @Route("/movie/search", name="movie_search")
     * @param Request $request
     * @return Response
     */
    public function search(Request $request): Response
    {
        $keyword = $request->query->get('keyword','love');
        $movies = $this->omdbApi->requestAllBySearch($keyword);


        return $this->render('movie/search.html.twig', [
            'movies' => $movies,
            'keyword' => $keyword
        ]);
    }


}
