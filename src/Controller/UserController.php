<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Event\UserRegisteredEvent;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request,
                             EntityManagerInterface $entityManager,
                             UserPasswordHasherInterface $passwordHasher,
                             EventDispatcherInterface $eventDispatcher
    ): Response
    {
        if ($this->getUser()) {
            $this->addFlash('danger','🚨 tu es déja connecté ! Il est impossible de créer un compte si tu es connecté');
           return $this->redirectToRoute('homepage');
        }
        $form = $this->createForm(UserType::class);
        //$form->add('save', SubmitType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var User $user */
           $user = $form->getData();
           $user->setPassword($passwordHasher->hashPassword($user,$user->getPassword()));
           $entityManager->persist($user);
           $entityManager->flush();
           $eventDispatcher->dispatch(new UserRegisteredEvent($user), 'user_registered');
        }


        return $this->render('user/register.html.twig',[
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(): Response
    {
        return $this->render('user/signin.html.twig');
    }
}
