<?php

namespace App\Command;

use App\Entity\Movie;
use App\OmdbApi;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MovieImportCommand extends Command
{
    protected static $defaultName = 'app:movie:import';
    protected static $defaultDescription = 'Add a short description for your command';

    private OmdbApi $omdbApi;

    private EntityManagerInterface $entityManager;


    public function __construct(OmdbApi $omdbApi, EntityManagerInterface $entityManager)
    {
        $this->omdbApi = $omdbApi;
        $this->entityManager = $entityManager;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption('keyword', 'k', InputOption::VALUE_REQUIRED, 'Mot-clé de recherche de films pour l\'import en bdd')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $keyword = $input->getOption('keyword');
        if (!$keyword) {
            $keyword = $io->ask('🧐 Vous avez oublié de préciser le mot-clé! Veuillez en choisir un','Harry Potter');
        }


        $io->title('# Films à importer en base de données avec le mot-clé '. $keyword);

        $movies = $this->omdbApi->requestAllBySearch($keyword);
        $io->progressStart(count($movies['Search']));
        foreach ($movies['Search'] as $movieData) {

            $movieData = $this->omdbApi->requestOneById($movieData["imdbID"]);

            $movie = Movie::fromApi($movieData);
            $this->entityManager->persist($movie);
            $io->progressAdvance();

        }
        $this->entityManager->flush();

        $output->write("\r");//permet de retirer la progress bar une fois à 100%
       // $io->progressFinish();

        $io->success(sprintf('%d films viennent d\'être importés',count($movies['Search']) ));

        return Command::SUCCESS;
    }
}
