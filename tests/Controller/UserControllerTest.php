<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'SensioTV+');
    }

    public function testUserRegistration(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertSelectorTextContains('h1', 'Create your account');

        //On failure
        $client->submitForm('user_save', []);
        $this->assertEquals(5, $client->getCrawler()->filter('.form-error-message')->count());

        //OnSuccess
        $client->submitForm('user_save', [
            'user[firstname]' => 'Yohan',
            'user[lastname]' => 'Legrand',
            'user[email]' => 'yohan.lgd@gmail.com',
            'user[password][first]' => 'monsupermotdepasse',
            'user[password][second]' => 'monsupermotdepasse',
            'user[terms]' => true,

        ]);
        // pour voir le resultat dans une vue static (faire "http://localhost:8000/test.html" pour voir le resultat du test )file_put_contents(__DIR__.'/../../public/test.html', print_r($client->getResponse()->getContent(), true));die;
        $this->assertEquals(0, $client->getCrawler()->filter('.form-error-message')->count());
        $userRepo = $client->getContainer()->get(UserRepository::class);
        $user = $userRepo->findOneByEmail('yohan.lgd@gmail.com');
    }
}
